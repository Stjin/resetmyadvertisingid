package host.stjin.resetmyadvertisingid

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity


class LaunchGMSAdsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val googleSettings = Intent("com.google.android.gms.settings.ADS_PRIVACY")
        startActivity(googleSettings)
        finish()
    }
}