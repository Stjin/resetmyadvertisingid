package host.stjin.resetmyadvertisingid

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.preference.PreferenceManager
import androidx.work.*
import com.google.common.util.concurrent.ListenableFuture
import java.util.*
import java.util.concurrent.ExecutionException
import java.util.concurrent.TimeUnit

class ScheduleHelper(private val context: Context) {
    private val TAGBACKUP: String = "TASK_host.stjin.resetmyadvertisingid"

    private var prefs: SharedPreferences

    init {
        prefs = PreferenceManager.getDefaultSharedPreferences(context)
    }

    fun initialSchedule(HOUR: Int, MINUTE: Int) {
        /*
        This will take care of the first execution. We need then to enqueue the next execution of this work when we complete successfully:
         */
        val currentDate = Calendar.getInstance()
        val dueDate = Calendar.getInstance()
        // Set Execution around 05:00:00 AM
        dueDate.set(Calendar.HOUR_OF_DAY, HOUR)
        dueDate.set(Calendar.MINUTE, MINUTE)
        dueDate.set(Calendar.SECOND, 0)
        if (dueDate.before(currentDate)) {
            dueDate.add(Calendar.HOUR_OF_DAY, 24)
        }
        val timeDiff = dueDate.timeInMillis - currentDate.timeInMillis
        val dailyWorkRequest = OneTimeWorkRequestBuilder<DailyWorker>()
            .setInitialDelay(timeDiff, TimeUnit.MILLISECONDS)
            .addTag(TAGBACKUP).build()
        WorkManager.getInstance(context).enqueue(dailyWorkRequest)
    }

    private fun isWorkScheduled(tag: String): Boolean {
        val instance = WorkManager.getInstance()
        val statuses: ListenableFuture<List<WorkInfo>> = instance.getWorkInfosByTag(tag)
        return try {
            var running = false
            val workInfoList: List<WorkInfo> = statuses.get()
            for (workInfo in workInfoList) {
                val state = workInfo.state
                running = state == WorkInfo.State.RUNNING || state == WorkInfo.State.ENQUEUED
            }
            running
        } catch (e: ExecutionException) {
            e.printStackTrace()
            false
        } catch (e: InterruptedException) {
            e.printStackTrace()
            false
        }
    }

    fun scheduleReminder() {
        prefs = PreferenceManager.getDefaultSharedPreferences(context)
        if (prefs.getBoolean("SCHEDULE", false)) {
            // check if your work is not already scheduled
            if (!isWorkScheduled(TAGBACKUP)) {
                initialSchedule(
                    prefs.getInt("SCHEDULE_HOUR", 0),
                    prefs.getInt("SCHEDULE_MINUTE", 0)
                )
            } else {
                WorkManager.getInstance(context).cancelAllWorkByTag(TAGBACKUP)
                initialSchedule(
                    prefs.getInt("SCHEDULE_HOUR", 0),
                    prefs.getInt("SCHEDULE_MINUTE", 0)
                )
            }
        }
    }
}

class DailyWorker(private val ctx: Context, params: WorkerParameters) : Worker(ctx, params) {
    override fun doWork(): Result {
        createNotification()
        ScheduleHelper(ctx).scheduleReminder()
        return Result.success()
    }

    private fun createNotification() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the NotificationChannel
            val name = ctx.resources.getString(R.string.app_name)
            val descriptionText = ctx.resources.getString(R.string.channel_description)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val mChannel = NotificationChannel(BuildConfig.APPLICATION_ID, name, importance)
            mChannel.description = descriptionText
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            val notificationManager = ctx.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(mChannel)
        }

        NotificationManagerCompat.from(ctx).apply {
            // Create an explicit intent for an Activity in your app
            val intent = Intent(ctx, LaunchGMSAdsActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }
            val pendingIntent: PendingIntent = PendingIntent.getActivity(ctx, 0, intent, 0)

            val builder = NotificationCompat.Builder(ctx, BuildConfig.APPLICATION_ID)
                .setSmallIcon(R.drawable.ic_google)
                .setContentTitle(ctx.resources.getString(R.string.reminder_title))
                .setContentText(ctx.resources.getString(R.string.reminder_text))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                // Set the intent that will fire when the user taps the notification
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)

            notify(0, builder.build())
        }
    }
}